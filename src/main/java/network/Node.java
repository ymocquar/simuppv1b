//
// Copyright 2015-2019 Yves Mocquard
//
//
// This file is a part of SimuPPv1b
// SimuPPv1b is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
package network;

import java.util.ArrayList;
import java.util.List;

public class Node {
	private final int id;
	private final IValue value;
	private final List<Node> neighbors;
	private boolean ok;
	private boolean fullConnected;
	private int fmrValue;

	public int getFmrValue() {
		return fmrValue;
	}

	public void setFmrValue(int fmrValue) {
		this.fmrValue = fmrValue;
	}

	/**
	 * get the value of ok flag
	 * 
	 * @return the value of ok
	 */
	public boolean isOk() {
		return ok;
	}

	/**
	 * get the value of ok flag
	 * 
	 * @param the
	 *            value of ok
	 */
	public void setOk(boolean ok) {
		this.ok = ok;
	}

	/**
	 * contructor
	 * 
	 * @param id
	 *            idenfier
	 * @param value
	 *            value of the Node
	 * @param r
	 *            Random generator
	 */
	Node(int id, IValue value, boolean fullConnected) {
		super();
		this.id = id;
		this.value = value;
		this.neighbors = new ArrayList<>();
		this.ok = false;
		this.fullConnected = fullConnected;
	}

	private List<Integer> getIdsNB() {
		List<Integer> liste = new ArrayList<Integer>();

		for (Node node : neighbors) {
			liste.add((Integer) node.id);
		}

		return liste;

	}

	@Override
	public String toString() {
		return "Node [id=" + id + ", value=" + value + ", neighbors=" + ((fullConnected) ? "All" : getIdsNB()) + "]";
	}

	/**
	 * get the neighbors list
	 * 
	 * @return
	 */
	public List<Node> getNeighbors() {
		return (fullConnected) ? null : neighbors;
	}

	/**
	 * get the Id
	 * 
	 * @return the Id
	 */
	public int getId() {
		return id;
	}

	/**
	 * ste the value
	 * 
	 * @return the value
	 */
	public IValue getValue() {
		return value;
	}

	/**
	 * test is other node is neighbor of this
	 * 
	 * @param other
	 *            node
	 * @return true is other is in the neighbors list
	 */
	public boolean isNeighbor(Node other) {
		return (fullConnected) ? (other != this) : neighbors.contains(other);
	}

}
