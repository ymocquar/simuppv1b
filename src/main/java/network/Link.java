//
// Copyright 2015-2019 Yves Mocquard
//
//
// This file is a part of SimuPPv1b
// SimuPPv1b is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
package network;

public class Link<T> {
	private T from;
	private T to;
	public T getFrom() {
		return from;
	}
	public void setFrom(T from) {
		this.from = from;
	}
	public T getTo() {
		return to;
	}
	public void setTo(T to) {
		this.to = to;
	}
	public Link(T from, T to) {
		super();
		this.from = from;
		this.to = to;
	}

}
