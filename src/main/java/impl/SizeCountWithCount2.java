//
// Copyright 2015-2019 Yves Mocquard
//
//
// This file is a part of SimuPPv1b
// SimuPPv1b is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
//
// Ce logiciel a été écrit par Yves Mocquard 
// 
// AverageLongWithCount
//
// Objet de base pour le protocole basé sur la moyenne
//
// un autre objet est défini à l'interieur : il s'agi d'un objet implémentant le cheduler 
// et gardera les donne globale 

package impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import log.Log;
import network.IValue;
import network.Network;
import network.NetworkFactory;
import network.Scheduler;

public class SizeCountWithCount2 implements IValue {

	static public class DataCountSizeCount implements Scheduler {
		private Random random;
		public final int n;
		public final long m;

		public double getEll() {
			return ((double) nb_A) * ((double) m) / ((double) n);
		}

		public double getEll(int nA) {
			return ((double) nA) * ((double) m) / ((double) n);
		}

		@Override
		public String toString() {
			return "DataCountSizeCount [n=" + n + ", m=" + m + ", nb_A=" + nb_A + ", nbGood=" + nbGood + "]";
		}

		private int nb_A = 0;
		private int nbGood = 0;

		private long minC = 0;
		private long maxC = 0;
		public double ell = 0.;
		public double result = 0.;

		public DataCountSizeCount(int n, long m, long seed) {
			random = new Random();
			this.n = n;
			this.m = m;
			setSeed(seed);
		}

		public boolean isConverge() {
			return nbGood == n;
		}

		private void setMinMax() {
			ell = ((double) m)*nb_A/n;
			if( Math.abs(ell-Math.floor(ell)-0.5) < 0.0000000001 )
			{
				minC = (long) Math.floor(ell);
				maxC = minC + 1;

			}
			else
			{
				minC = (long) Math.floor(ell + 0.5) - 1;
				maxC = minC + 2;
			}
			if ( minC <= 0 ) nbGood = n-1;

		}

		public long getMinC() {
			return minC;
		}

		public long getMaxC() {
			return maxC;
		}

		public void setProp() {

			setMinMax();
		}
		
		public long min(long c)
		{
			return (long)Math.ceil(((double)m)*2./(2.*c+3));
		}
		public long max(long c)
		{
			if ( c <= 1 ) return Long.MAX_VALUE;
			return (long)Math.floor(((double)m)*2./(2.*c-3));
		}

		@Override
		public int nextInt(int borne) {
			return random.nextInt(borne);
		}

		@Override
		public boolean nextBoolean() {
			return random.nextBoolean();
		}

		@Override
		public void setSeed(long seed) {
			random.setSeed(seed);
		}
	}

	// Méthode appelée par le stream
	// Cette méthode lance un protocole.
	// random implemente l'interface Scheduler, c'est ala fois un genrateur
	// aleatoire et aussi
	// permet de centralisé les donnée propore à ce protocole comme le nombre
	// d'agents ayant le bon résultat.
	// Le but étant de détecter précisément l'instant de la convergence.
	//
	public static void executePP(SizeCountWithCount2.DataCountSizeCount random) {
		List<IValue> values = new ArrayList<>();
		int n = random.n;

		NetworkFactory fact = NetworkFactory.getInstance(n);
		for (int i = 0; i < n; i++) {
			values.add(new SizeCountWithCount2(random, i == 0));
		}
		random.setProp();

		fact.setRandom(random);
		fact.setValues(values);
		fact.setFullConnected(true);

		Network netWork = fact.getInstanceOfNetWork();
		long borneSec = (long) (Math.log(n) * 100 * n);
		long k = 0;
		while (!random.isConverge()) {
			netWork.process1();
			k++;
			if (k > borneSec) {
				Log.getInstance().error("ERREUR k=" + k + " borneSec =" + borneSec + " r=" + random);
				Log.getInstance().error("ABORT");
				System.exit(-1);
				break;
			}
		}
		long minVal = values.parallelStream().mapToLong(v -> ((SizeCountWithCount2) v).val).min().getAsLong();
		long maxVal = values.parallelStream().mapToLong(v -> ((SizeCountWithCount2) v).val).max().getAsLong();
		if (maxVal - minVal > 2) {
			Log.getInstance().error("ERROR min=" + minVal + " max=" + maxVal + " r=" + random);
			Log.getInstance().error("ABORT");
			System.exit(-1);
		}
		random.result = ((double) k) / ((double) n);
	}

	DataCountSizeCount refDC;

	public SizeCountWithCount2(DataCountSizeCount dc, boolean isA) {
		refDC = dc;
		if (isA) {
			val = refDC.m;
			refDC.nb_A++;
		} else {
			val = 0;
		}
	}

	private boolean isGood() {
		// return (float) Math.abs(((double) val) / refDC.m - refDC.prop) < (float)
		// refDC.eps;
		
		return refDC.n <= refDC.max(val) && refDC.n >= refDC.min(val);
	}

	public long val;

	@Override
	public void process(IValue value) {
		SizeCountWithCount2 v = (SizeCountWithCount2) value;

		int locNbGood = (isGood() ? 1 : 0) + (v.isGood() ? 1 : 0);

		long sum = v.val + val;
		v.val = sum / 2;
		val = sum - v.val;

		int plus = (isGood() ? 1 : 0) + (v.isGood() ? 1 : 0) - locNbGood;

		refDC.nbGood += plus;

	}

}
