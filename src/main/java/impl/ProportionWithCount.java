//
// Copyright 2015-2019 Yves Mocquard
//
//
// This file is a part of SimuPPv1b
// SimuPPv1b is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
//
// Ce logiciel a été écrit par Yves Mocquard 
// 
// AverageLongWithCount
//
// Objet de base pour le protocole basé sur la moyenne
//
// un autre objet est défini à l'interieur : il s'agi d'un objet implémentant le cheduler 
// et gardera les donne globale 

package impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import log.Log;
import network.IValue;
import network.Network;
import network.NetworkFactory;
import network.Scheduler;

public class ProportionWithCount implements IValue {

	static public class DataCountProportion implements Scheduler {
		private Random random;
		public final int n;
		public final int m;

		public static int getM(double eps) {
			return ((int) Math.ceil(3. / (2. * eps)));
		}

		// getPireProp : cette fonction rend la proportion la plus proche de 0.5
		// qui rend un ell dont la partie fractionnaire est égale à 0.5
		static public double getPireProp(double eps, int n) {
			int m = ((int) Math.ceil(3. / (2. * eps)));
			int memI = 0;
			int diff = 0;
			int bestDiff = n;
			int objectif = n / 2;

			for (int i = n / 2; i > 0; i--) {
				int mod = (int) ((((long) i) * ((long) m) % ((long) n)));

				diff = Math.abs(mod - objectif);
				if (diff < bestDiff) {
					if (diff == 0)
						return ((double) i) / ((double) n);
					bestDiff = diff;
					memI = i;
				}
			}
			return ((double) memI) / ((double) n);
		}

		public double getEll() {
			return ((double) nb_A) * ((double) m) / ((double) n);
		}

		public double getEll(int nA) {
			return ((double) nA) * ((double) m) / ((double) n);
		}

		public static double getEll(double prop, double eps) {
			return prop * ((double) getM(eps));
		}

		@Override
		public String toString() {
			return "DataCountProportion [n=" + n + ", m=" + m + ", nb_A=" + nb_A + ", nbGood=" + nbGood + ", result="
					+ result + "]";
		}

		public double prop = 0.;
		public double ell = 0.;
		public double eps;
		private int nb_A = 0;
		private int nbGood = 0;
		public double result = 0;

		private int minC = 0;
		private int maxC = 0;

		public static double getFract(double v) {
			return v - Math.floor(v);
		}

		public DataCountProportion(int n, double eps, long seed) {
			random = new Random();
			this.n = n;
			this.m = ((int) Math.ceil(3. / (2. * eps)));
			this.eps = eps;
			setSeed(seed);
		}

		public boolean isConverge() {
			return nbGood == n;
		}

		private void setMinMax() {
			int mod = (int) (((long) nb_A) * ((long) m) % ((long) n));
			minC = (int) (((long) nb_A) * ((long) m) / ((long) n));
			maxC = minC + 1;
			boolean flInit = false;

			flInit = (n % 2 == 0) ? (mod == n / 2) : (mod == (n + 1) / 2 || mod == (n - 1) / 2);
			if (!flInit) {
				if (mod > n / 2) {
					maxC++;
				} else {
					minC--;
				}
			}

		}
		public int getMinC() {
			return minC;
		}
		public int getMaxC() {
			return maxC;
		}

		public void setProp() {
			prop = ((double) nb_A) / n;
			ell = prop * m;
			if (nb_A == n || nb_A == 0)
				nbGood = n;
			
			setMinMax();
		}

		@Override
		public int nextInt(int borne) {
			return random.nextInt(borne);
		}

		@Override
		public boolean nextBoolean() {
			return random.nextBoolean();
		}

		@Override
		public void setSeed(long seed) {
			random.setSeed(seed);
		}
	}

	// Méthode appelée par le stream
	// Cette méthode lance un protocole.
	// random implemente l'interface Scheduler, c'est ala fois un genrateur
	// aleatoire et aussi
	// permet de centralisé les donnée propore à ce protocole comme le nombre
	// d'agents ayant le bon résultat.
	// Le but étant de détecter précisément l'instant de la convergence.
	//
	public static void executePP(ProportionWithCount.DataCountProportion random, double prop) {
		List<IValue> values = new ArrayList<>();
		int n = random.n;

		NetworkFactory fact = NetworkFactory.getInstance(n);
		int borne = (int) Math.round(prop * ((double) n));
		for (int i = 0; i < n; i++) {
			values.add(new ProportionWithCount(random, i < borne));
		}
		random.setProp();

		fact.setRandom(random);
		fact.setValues(values);
		fact.setFullConnected(true);

		Network netWork = fact.getInstanceOfNetWork();
		long borneSec = (long) (Math.log(n) * 100 * n);
		long k = 0;
		while (!random.isConverge()) {
			netWork.process1();
			k++;
			if (k > borneSec) {
				Log.getInstance().error("ERREUR k=" + k + " borneSec =" + borneSec + " r=" + random);
				Log.getInstance().error("ABORT");
				System.exit(-1);
				break;
			}
		}
		int minVal = values.parallelStream().mapToInt(v -> ((ProportionWithCount) v).val).min().getAsInt();
		int maxVal = values.parallelStream().mapToInt(v -> ((ProportionWithCount) v).val).max().getAsInt();
		if (maxVal - minVal > 2) {
			Log.getInstance().error("ERROR min=" + minVal + " max=" + maxVal + " r=" + random);
			Log.getInstance().error("ABORT");
			System.exit(-1);
		}
		random.result = ((double) k) / ((double) n);
	}

	DataCountProportion refDC;

	public ProportionWithCount(DataCountProportion dc, boolean isA) {
		refDC = dc;
		if (isA) {
			val = refDC.m;
			refDC.nb_A++;
		} else {
			val = 0;
		}
	}

	private boolean isGood() {
		// return (float) Math.abs(((double) val) / refDC.m - refDC.prop) < (float) refDC.eps;
		return val <= refDC.maxC && val >= refDC.minC;
	}

	public int val;

	@Override
	public void process(IValue value) {
		ProportionWithCount v = (ProportionWithCount) value;

		int locNbGood = (isGood() ? 1 : 0) + (v.isGood() ? 1 : 0);

		int sum = v.val + val;
		v.val = sum / 2;
		val = sum - v.val;

		int plus = (isGood() ? 1 : 0) + (v.isGood() ? 1 : 0) - locNbGood;

		refDC.nbGood += plus;

	}

}
