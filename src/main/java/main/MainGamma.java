//
// Copyright 2015-2019 Yves Mocquard
//
//
// This file is a part of SimuPPv1b
// SimuPPv1b is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
//
// Ce logiciel a été écrit par Yves Mocquard en 2019
// 
// MainCounting
//
// Ce logiciel générer des données utilisables par des graphiques
// Il fonctionne en multithread
// le multithread est basé sur la technologie Stream.
//

package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import impl.ProportionWithCount;
import log.Log;

public class MainGamma {

	// classe interne permettant de stocker les résultats
	static class Result {
		int n;
		double ell;
		double gammaA;
		double moy;
		List<Double> res = new ArrayList<>();
	}

	static long seed = 0;
	static boolean isForced = false;
	static int n_st;

	static String strForced() {
		return isForced ? " seed forcé" : "";
	}

	static String getSignature() {
		return "\t# MainGamma v1.0 seed=" + seed + strForced();
	}

	static String getSignature(int nbT) {
		return getSignature() + " N=" + nbT + " n="+n_st+ strForced();
	}

	// Méthode main
	// Il y a un parametre optionnel c'est le seed qui permet de reproduire
	// exactement la même simulation
	//
	// sans paramètre un seed est généré automatiquement.
	public static void main(String[] args) {
		int nbTest = 1000000;
		int paquetTest = 100000;
		Random rand = new Random();
		int n = 1000;
		n_st=n;
		double eps = 0.0075; // pour avoir m = 200
		int m = ProportionWithCount.DataCountProportion.getM(eps);

		Result[] tabResult = new Result[99];

		// Result[] tabResult = new Result[tab_n.length];

		seed = (args.length == 1) ? Long.parseLong(args[0]) : rand.nextLong();

		seed &= (1L << 32) - 1L;

		rand.setSeed(seed);

		isForced = (args.length == 1);

		for (int i_nA = 0; i_nA < 99; i_nA++) {
			int nA = (i_nA+1)*10;
			Result res = new Result();
			tabResult[i_nA] = res;
			res.n = n;
			res.ell = ((double)(nA*m))/((double)n);
			res.gammaA = ((double)nA)/((double)n);
			for (int nbT = 0; nbT < nbTest; nbT += paquetTest) {

				List<ProportionWithCount.DataCountProportion> tab = new ArrayList<>();
				for (int i = 0; i < paquetTest; i++) {
					tab.add(new ProportionWithCount.DataCountProportion(n, eps, rand.nextLong()));
				}
				final double prop = ((double) nA) / ((double) n);
				tab.parallelStream().forEach(r -> ProportionWithCount.executePP(r, prop));

				for (ProportionWithCount.DataCountProportion s : tab) {
					res.res.add(s.result);

				}
				Log.getInstance().info(" n=" + n + " nbT=" + nbT + "/" + nbTest);
			}
			Collections.sort(res.res);
			res.moy=0.;
			for( Double d: res.res )
			{
				res.moy += d;
			}
			res.moy /= res.res.size();


			Log.getInstance().info("\t#gammaA\tell\tt median\tt moyen" + getSignature());
			for (int i = 0; i <= i_nA; i++) {
				Result r = tabResult[i];

				String str = "\t" + ((float) r.gammaA)+"\t" + ((float) r.ell);


				str += "\t" + (float)((r.res.get(r.res.size() / 2) + r.res.get(r.res.size() / 2 - 1)) / 2.);
				str += "\t" + (float)r.moy;

				Log.getInstance().info(str + getSignature(r.res.size()));

			}

		}
	}

}
