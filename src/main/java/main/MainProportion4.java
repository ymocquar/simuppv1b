//
// Copyright 2015-2019 Yves Mocquard
//
//
// This file is a part of SimuPPv1b
// SimuPPv1b is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
//
// Ce logiciel a été écrit par Yves Mocquard en 2019
// 
// MainCounting
//
// Ce logiciel générer des données utilisables par des graphiques
// Il fonctionne en multithread
// le multithread est basé sur la technologie Stream.
//

package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import impl.ProportionWithCount;
import log.Log;

public class MainProportion4 {

	// classe interne permettant de stocker les résultats
	static class Result {
		int n;
		List<Double> resPire = new ArrayList<>();
	}

	static long seed=0;
	static boolean isForced=false;
	
	static String strForced() {
		return isForced ?" seed forcé":"";
	}
	
	static String getSignature()
	{
		return "\t# MainProportion4 v1.0 seed=" + seed+strForced();
	}
	static String getSignature( int nbT )
	{
		return getSignature()+" N="+nbT+strForced();
	}

	// Méthode main
	// Il y a un parametre optionnel c'est le seed qui permet de reproduire
	// exactement la même simulation
	//
	// sans paramètre un seed est généré automatiquement.
	public static void main(String[] args) {
		int nbTest = 1000;
		int paquetTest = 100;
		Random rand = new Random();
		int[] tab_n = { 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000,
				1000000, 2000000, 5000000, 10000000 };
		double[] tabEps = { 0.1, 0.01, 0.001, 0.0001 };

		Result[][] tabResult = new Result[tab_n.length][tabEps.length];

		// Result[] tabResult = new Result[tab_n.length];

		seed = (args.length == 1) ? Long.parseLong(args[0]) : rand.nextLong();

		seed &= (1L << 32) - 1L;

		rand.setSeed(seed);

		isForced =  (args.length == 1);

		for (int i_n = 0; i_n < tab_n.length; i_n++) {
			int n = tab_n[i_n];

			if (n >= 1000000) {
				nbTest = 100;
				paquetTest = 10;
			}

			for (int i_eps = 0; i_eps < tabEps.length; i_eps++) {

				Result res = new Result();
				tabResult[i_n][i_eps] = res;
				res.n = n;

				double eps = tabEps[i_eps];
				double eps_2=eps;
//				double prop = ProportionWithCount.DataCountProportion.getPireProp(eps, n);
//				Log.getInstance().info(" n=" + n + " eps=" + eps + " pire prop=" + prop+ "ell pire="+ProportionWithCount.DataCountProportion.getEll(prop, eps));

				if ( ProportionWithCount.DataCountProportion.getM(eps) % 2 == 0 )
				{
					eps_2 = eps -eps*eps/2.;
					if ( ProportionWithCount.DataCountProportion.getM(eps_2) -ProportionWithCount.DataCountProportion.getM(eps) != 1)
					{
						Log.getInstance().error("eps="+eps+" ---> m="+ProportionWithCount.DataCountProportion.getM(eps));
						Log.getInstance().error("eps-eps*eps/2.="+(eps-eps*eps/2.)+" ---> m="+ProportionWithCount.DataCountProportion.getM(eps-eps*eps/2.));
						Log.getInstance().error("ABORT");
						System.exit(-1);
					}
		
				}
				
				for (int nbT = 0; nbT < nbTest; nbT += paquetTest) {

					List<ProportionWithCount.DataCountProportion> tab = new ArrayList<>();
					for (int i = 0; i < paquetTest; i++) {
						tab.add(new ProportionWithCount.DataCountProportion(n, eps_2, rand.nextLong()));
					}
					tab.parallelStream().forEach(r -> ProportionWithCount.executePP(r, 0.5));

					for (ProportionWithCount.DataCountProportion s : tab) {
						res.resPire.add(s.result);
					}
					Log.getInstance().info(" n=" + n + " nbT=" + nbT + "/" + nbTest);
				}
				Collections.sort(res.resPire);

				String strEps = "";
				for ( int k=0 ; k < tabEps.length ; k++) {
					strEps += "\t" + tabEps[k];
				}

				Log.getInstance().info("\t# n" + strEps + getSignature());
				for (int i = 0; i <= i_n; i++) {
					String str = "\t" + tab_n[i];
					Result r=null;
					for (int j = 0; j < ((i == i_n) ? i_eps + 1 : tabEps.length); j++) {

						r = tabResult[i][j];
						int indice = r.resPire.size() - (int) Math.round(0.1 * r.resPire.size());

						str += "\t" + r.resPire.get(indice);

					}
					Log.getInstance().info(str + getSignature(r.resPire.size()));

				}
			}

		}
	}
}
