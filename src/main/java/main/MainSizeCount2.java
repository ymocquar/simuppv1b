//
// Copyright 2015-2019 Yves Mocquard
//
//
// This file is a part of SimuPPv1b
// SimuPPv1b is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
//
// Ce logiciel a été écrit par Yves Mocquard en 2019
// 
// MainCounting
//
// Ce logiciel générer des données utilisables par des graphiques
// Il fonctionne en multithread
// le multithread est basé sur la technologie Stream.
//

package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import impl.SizeCountWithCount2;
import log.Log;

public class MainSizeCount2 {

	// classe interne permettant de stocker les résultats
	static class Result {
		int n;
		long m;;
		List<Double> res = new ArrayList<>();
	}

	static long seed = 0;
	static boolean isForced = false;

	static String strForced() {
		return isForced ? " seed forcé" : "";
	}

	static String getSignature() {
		return "\t# MainSizeCount2 v1.0 seed=" + seed + strForced();
	}

	static String getSignature(int nbT) {
		return getSignature() + " N=" + nbT + strForced();
	}

	// Méthode main
	// Il y a un parametre optionnel c'est le seed qui permet de reproduire
	// exactement la même simulation
	//
	// sans paramètre un seed est généré automatiquement.
	public static void main(String[] args) {
		int nbTest = 100000;
		int paquetTest = 10000;
		Random rand = new Random();
		long[] tabM = { 10,20,50,100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000, 1000000, 2000000,
				5000000, 10000000, 20000000,
				50000000, 100000000, 200000000,
				500000000, 1000000000 };
		int[] tab_n = { 1000 };

		Result[][] tabResult = new Result[tabM.length][tab_n.length];

		// Result[] tabResult = new Result[tab_n.length];

		seed = (args.length == 1) ? Long.parseLong(args[0]) : rand.nextLong();

		seed &= (1L << 32) - 1L;

		rand.setSeed(seed);

		isForced = (args.length == 1);
		for (int i_m = 0; i_m < tabM.length; i_m++) {
			for (int i_n = 0; i_n < tab_n.length; i_n++) {
				long m = tabM[i_m];
				Result res = new Result();
				tabResult[i_m][i_n] = res;
				int n = tab_n[i_n];
				res.n = n;
				res.m = m;

				for (int nbT = 0; nbT < nbTest; nbT += paquetTest) {

					List<SizeCountWithCount2.DataCountSizeCount> tab = new ArrayList<>();
					for (int i = 0; i < paquetTest; i++) {
						tab.add(new SizeCountWithCount2.DataCountSizeCount(n, m, rand.nextLong()));
					}
					tab.parallelStream().forEach(r -> SizeCountWithCount2.executePP(r));

					for (SizeCountWithCount2.DataCountSizeCount s : tab) {
						res.res.add(s.result);
					}
					Log.getInstance().info(" n=" + n + " nbT=" + nbT + "/" + nbTest);
				}
				Collections.sort(res.res);

				String strM = "";
//				for (int k = 0; k < 4; k++) {
//					strM += "\t" + tab_n[k];
//				}

				Log.getInstance().info("\t# m" + strM + getSignature());
				for (int i = 0; i <= i_m; i++) {
					String str = "\t" + tabM[i];
					Result r = tabResult[i][0];
//
					double delta = 0.1;
					for (int j = 0; j < 4 ; j++ , delta*=0.1) {

						r = tabResult[i][0];
						int indice = r.res.size() - (int) Math.floor(delta * r.res.size() - 0.5);

						str += "\t" + r.res.get(indice);

					}
					Log.getInstance().info(str + getSignature(r.res.size()));

				}
			}

		}
	}
}
