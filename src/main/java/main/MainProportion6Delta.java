//
// Copyright 2015-2019 Yves Mocquard
//
//
// This file is a part of SimuPPv1b
// SimuPPv1b is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
//
// Ce logiciel a été écrit par Yves Mocquard en 2019
// 
// MainCounting
//
// Ce logiciel générer des données utilisables par des graphiques
// Il fonctionne en multithread
// le multithread est basé sur la technologie Stream.
//

package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import impl.ProportionWithCount;
import log.Log;

public class MainProportion6Delta {

	// classe interne permettant de stocker les résultats
	static class Result {
		int n;
		double eps;
		List<Double> resPire = new ArrayList<>();
	}

	static long seed = 0;
	static boolean isForced = false;

	static final int n = 1000;

	static String strForced() {
		return isForced ? " seed forcé" : "";
	}

	static String getSignature() {
		return "\t# MainProportion6Delta v1.0 seed=" + seed + strForced();
	}

	static String getSignature(int nbT) {
		return getSignature() + " N=" + nbT + strForced();
	}

	// Méthode main
	// Il y a un parametre optionnel c'est le seed qui permet de reproduire
	// exactement la même simulation
	//
	// sans paramètre un seed est généré automatiquement.
	public static void main(String[] args) {
		int nbTest = 1000000;
		int paquetTest = 100000;
		Random rand = new Random();

		double[] tab_delta = { 0.1, 0.001, 0.00001 };

		Result[] tabResult = new Result[21];

		// Result[] tabResult = new Result[tab_n.length];

		seed = (args.length == 1) ? Long.parseLong(args[0]) : rand.nextLong();

		seed &= (1L << 32) - 1L;

		rand.setSeed(seed);

		isForced = (args.length == 1);

		double[] tabMult = { 2.5, 2., 2. };

		int i_eps = 0;
		for (double eps = 0.1; i_eps < tabResult.length; eps /= tabMult[(i_eps + 2) % 3], i_eps++) {
			double eps_2 = eps;
//			double prop = ProportionWithCount.DataCountProportion.getPireProp(eps, n);
//			Log.getInstance().info(" n=" + n + " eps=" + eps + " pire prop=" + prop+ "ell pire="+ProportionWithCount.DataCountProportion.getEll(prop, eps));

			if (ProportionWithCount.DataCountProportion.getM(eps) % 2 == 0) {
				eps_2 = eps - eps * eps / 2.;
				if (ProportionWithCount.DataCountProportion.getM(eps_2)
						- ProportionWithCount.DataCountProportion.getM(eps) != 1) {
					Log.getInstance()
							.error("eps=" + eps + " ---> m=" + ProportionWithCount.DataCountProportion.getM(eps));
					Log.getInstance().error("eps-eps*eps/2.=" + (eps - eps * eps / 2.) + " ---> m="
							+ ProportionWithCount.DataCountProportion.getM(eps - eps * eps / 2.));
					Log.getInstance().error("ABORT");
					System.exit(-1);
				}

			}

			Result res = new Result();
			tabResult[i_eps] = res;
			res.n = n;
			res.eps = eps;

			for (int nbT = 0; nbT < nbTest; nbT += paquetTest) {

				List<ProportionWithCount.DataCountProportion> tab = new ArrayList<>();
				for (int i = 0; i < paquetTest; i++) {
					tab.add(new ProportionWithCount.DataCountProportion(n, eps_2, rand.nextLong()));
				}
				tab.parallelStream().forEach(r -> ProportionWithCount.executePP(r, 0.5));

				for (ProportionWithCount.DataCountProportion s : tab) {
					res.resPire.add(s.result);
				}
				Log.getInstance().info(" n=" + n + " eps="+eps+" nbT=" + nbT + "/" + nbTest);
			}
			Collections.sort(res.resPire);

			String str_n = "# eps";
			for (int i = 0; i < tab_delta.length; i++)
				str_n += "\t" + tab_delta[i];

			Log.getInstance().info("\t" + str_n + getSignature());

			for (int i = 0; i <= i_eps; i++) {
				Result r = tabResult[i];

				String str = "\t" + ((float) r.eps);
				for (int j = 0; j < tab_delta.length; j++) {

					str += "\t" + r.resPire.get((int) Math.round((1. - tab_delta[j]) * nbTest));

				}
				Log.getInstance().info(str + getSignature(nbTest));
			}

		}

	}
}
