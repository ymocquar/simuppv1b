//
// Copyright 2015-2019 Yves Mocquard
//
//
// This file is a part of SimuPPv1b
// SimuPPv1b is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
package network;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestLink {
	
	Integer v1;
	Integer v2;
	Link<Integer> link;
	
	@Before
	public void setUp()
	{
		v1= new Integer(1);
		v2= new Integer(2);
		
		link = new Link<>(v1,v2);		
	}
	
	@Test
	public void testGetFromTo()
	{
		assertEquals(1,(int)link.getFrom());
		assertEquals(2,(int)link.getTo());
	}
	@Test
	public void testSetFromTo()
	{
		link.setFrom(78);
		link.setTo(7789);
		assertEquals(78,(int)link.getFrom());
		assertEquals(7789,(int)link.getTo());
	}

}
