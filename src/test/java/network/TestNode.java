//
// Copyright 2015-2019 Yves Mocquard
//
//
// This file is a part of SimuPPv1b
// SimuPPv1b is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
package network;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import impl.HowMany;

public class TestNode {
	
	Node node;
	Node nodeFull;
	IValue val=new HowMany();
	
	Node [] neighbors=new Node[10];
	IValue [] values= new HowMany[10];
	
	
	@Before
	public void setUp()
	{
		node = new Node(100,val,false);
		List<Node> list=node.getNeighbors();
		for ( int i = 0 ; i < 10 ; i++ )
		{
			HowMany howMany = new HowMany();
			howMany.setVal((double)(200+i));
			values[i] = howMany;
			neighbors[i] = new Node(200+i,howMany,false);
			if ( i < 5 )
			{
				list.add(neighbors[i]);
			}
		}
		nodeFull = new Node(100,val,true);
	}
	
	@Test
	public void testFisrt()
	{
		assertEquals(100,node.getId());
		assertTrue(node.isNeighbor(neighbors[4]));
		assertFalse(node.isNeighbor(neighbors[5]));
		List<Node> list = node.getNeighbors();
		assertEquals(5,list.size());
		assertEquals(neighbors[2],list.get(2));
		
//		assertEquals(203,node.getRandomNeighbor().getId());
		
		assertFalse(node.isOk());
		
		node.setOk(true);
		
		assertTrue(node.isOk());
		
		assertEquals("Node [id=100, value=HowMany [val=0.0], neighbors=[200, 201, 202, 203, 204]]",node.toString());
		
	}
	
	@Test
	public void testSecond()
	{
		List<Node> list = node.getNeighbors();
		
		list.clear();
		
//		assertNull(node.getRandomNeighbor());
		
		list.add(neighbors[0]);
		
//		assertEquals(200,node.getRandomNeighbor().getId());
		
		assertEquals(val,node.getValue());

	}
	
	@Test
	public void testFullConnect()
	{
		assertEquals("Node [id=100, value=HowMany [val=0.0], neighbors=All]",nodeFull.toString());
		assertNull(nodeFull.getNeighbors());
		assertFalse(nodeFull.isNeighbor(nodeFull));
		assertTrue(nodeFull.isNeighbor(neighbors[3]));
	}
	

}
