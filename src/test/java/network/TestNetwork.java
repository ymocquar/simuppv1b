//
// Copyright 2015-2019 Yves Mocquard
//
//
// This file is a part of SimuPPv1b
// SimuPPv1b is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
package network;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import impl.HowMany;

public class TestNetwork {

	NetworkFactory factory;

	Network network;

	@Before
	public void setUp() {
		factory = NetworkFactory.getInstance(100);

		List<IValue> list = getListValues(100);

		factory.setValues(list);

		factory.getRandom().setSeed(3456788901L);

		network = factory.getInstanceOfNetWork();

	}

	private List<IValue> getListValues(int nb) {
		List<IValue> list = new ArrayList<>();
		for (int i = 0; i < nb; i++) {
			HowMany howMany = new HowMany();
			howMany.setVal((double) i);
			list.add(howMany);
		}
		return list;
	}

	@Test
	public void testFirst() {
		List<Node> nodes = network.getNodes();
		assertEquals(100, nodes.size());
		assertEquals(3, nodes.get(0).getNeighbors().size());
		assertEquals(7, nodes.get(10).getNeighbors().size());
		assertEquals(4, nodes.get(50).getNeighbors().size());
		assertEquals(6, nodes.get(99).getNeighbors().size());
	}

	@Test
	public void testSecond() {
		factory.setNbNeighborMax(2);

		Network networkLoc = factory.getInstanceOfNetWork();

		List<Node> nodes = networkLoc.getNodes();

		assertEquals(100, nodes.size());
		assertEquals(5, nodes.get(0).getNeighbors().size());
		assertEquals(3, nodes.get(10).getNeighbors().size());
		assertEquals(1, nodes.get(50).getNeighbors().size());
		assertEquals(2, nodes.get(99).getNeighbors().size());

	}

	@Test
	public void testThird() {
		factory.setNbNeighborMax(100);

		Network networkLoc = factory.getInstanceOfNetWork();

		List<Node> nodes = networkLoc.getNodes();

		assertEquals(100, nodes.size());
		assertEquals(92, nodes.get(0).getNeighbors().size());
		assertEquals(49, nodes.get(10).getNeighbors().size());
		assertEquals(42, nodes.get(50).getNeighbors().size());
		assertEquals(82, nodes.get(99).getNeighbors().size());

	}

	@Test
	public void testProcess1() {
		network.process1(10000);

		for (Node n : network.getNodes()) {
			HowMany v = (HowMany) n.getValue();
			assertEquals(49.5, v.getVal(), 0.0001);
		}
	}

	@Test
	public void testProcess2() {
		network.process2(10000);

		for (Node n : network.getNodes()) {
			HowMany v = (HowMany) n.getValue();
			assertEquals(49.5, v.getVal(), 0.0001);
		}
	}

	@Test
	public void testProcess1Full() {
		factory.setFullConnected(true);
		Network networkLoc = factory.getInstanceOfNetWork();

		networkLoc.process1(10000);

		for (Node n : networkLoc.getNodes()) {
			HowMany v = (HowMany) n.getValue();
			assertEquals(49.5, v.getVal(), 0.0001);
		}
	}

	@Test
	public void testProcess2Full() {
		factory.setFullConnected(true);
		Network networkLoc = factory.getInstanceOfNetWork();
		networkLoc.process2(10000);

		for (Node n : networkLoc.getNodes()) {
			HowMany v = (HowMany) n.getValue();
			assertEquals(49.5, v.getVal(), 0.0001);
		}
	}

}
