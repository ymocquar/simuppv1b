#
# Copyright 2015-2019 Yves Mocquard
#
#
# This file is a part of SimuPPv1b
#
# SimuPPv1b is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#
set terminal epslatex
# set terminal pdf enhanced font "times,12"
set xlabel "$\\delta$"
set ylabel "Parallel convergence time"
set key left top Left reverse
set logscale x
set format x "$10^{%L}$"
# set output "resultProportion5Delta.pdf"
set output "resultProportion5Delta.tex"
# a = n,  b = delta, c = eps
oldfct(a,b,c)=3.12*log(a)-2*log(c)-6.59*log(b)+2.58 # Théorème 5.2.13 de la thèse p.80
fct(a,b,c)=log(a)-log(b)+2.*log(2.+1./c)+log(9./32.)
# fct(a,b,c)=log(a)-log(b)+2.*log(2.+1./c)+log(27./128.)
plot [0.5:0.00001] fct(100000,x, 0.01-0.00005) t "$\\tau$ with $n=10^5$" with line lt 1, "proportion5Delta.dat" using 1:4 title "$\\theta_{\\lceil R(1-\\delta)\\rceil}$ with $n=10^5$" with points lt 1, fct(10000,x, 0.01-0.00005) t "$\\tau$ with $n=10^4$" with line lt 2, "" using 1:3 title "$\\theta_{\\lceil R(1-\\delta)\\rceil}$ with $n=10^4$" with points lt 2,fct(1000,x, 0.01-0.00005) t "$\\tau$ with $n=10^3$" with line lt 3, "" using 1:2 title "$\\theta_{\\lceil R(1-\\delta)\\rceil}$ with $n=10^3$" with points lt 3

