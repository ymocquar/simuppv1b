#
# Copyright 2015-2019 Yves Mocquard
#
#
# This file is a part of SimuPPv1b
#
# SimuPPv1b is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#
set terminal epslatex
# set terminal pdf enhanced font "times,12"
set xlabel "$\\gamma_A$"
set ylabel "Parallel convergence time"
set key left top Left reverse
# set output "resultGamma.pdf"
set output "resultGamma.tex"
set xtics 0,0.1,1
plot [0:1] "gamma.dat" using 1:4 notitle
