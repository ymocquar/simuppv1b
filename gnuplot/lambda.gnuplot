#
# Copyright 2015-2019 Yves Mocquard
#
#
# This file is a part of SimuPPv1b
#
# SimuPPv1b is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#
set terminal epslatex
# set terminal pdf enhanced font "times,12"
# set yrange[14:19]
set xlabel "$\\ell$"
set ylabel "Parallel convergence time"
set key left top Left reverse
# set output "resultLambda.pdf"
set output "resultLambda.tex"
# a = n,  b = delta, c = eps
# plot [0:4] "lambda.dat" using 1:2 notitle
# plot [0:4] "lambda.dat" using 1:2 title "$\\tau$, $n=10000$", "lambdaOdd.dat" using 1:2 title "$\\tau$, $n=10001$"
plot [100:104] "lambda.dat" using 1:3 notitle
