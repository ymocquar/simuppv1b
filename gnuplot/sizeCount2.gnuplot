#
# Copyright 2015-2019 Yves Mocquard
#
#
# This file is a part of SimuPPv1b
#
# SimuPPv1b is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#
set terminal epslatex
# set terminal pdf enhanced font "times,12"
# set yrange[5:90]
set xlabel "m"
set ylabel "Parallel convergence time"
set key left top Left reverse
set logscale x
# set format x "$10^{%L}$"
set xtics ("$10^1$" 10,"$10^3$" 10**3,"$10^4$" 10**4,"$10^5$" 10**5,"$10^6$" 10**6,"$10^7$" 10**7,"$10^8$" 10**8)
# set output "sizeCount2.pdf"
set output "sizeCount2.tex"
# a = n,  b = delta, c = eps
borne1(n)=(2./log(2))*sqrt(n)
borne2(n)=3.*n*(n+1)


fct(n,x,d)=(x>borne1(n))?2.*log(x)-log(d):2.*log(x+sqrt(n))-log(d)-log(2)
set arrow from borne1(1000),5 to borne1(1000), fct(1000,borne1(1000),10**(-4)) nohead
set xtics add ("$m_1$" borne1(1000))
set arrow from borne2(1000),5 to borne2(1000), fct(1000,borne2(1000),10**(-4)) nohead
set xtics add ("$m_2$" borne2(1000))
plot [10:100000000] fct(1000,x,10**(-4)) t "$\\tau$" with line lt 1, "sizeCount2.dat" using 1:5 title "$\\theta_{\\lceil R(1-\\delta)\\rceil}$" with points lt 1

# ,fct(x,0.1,0.1) t "$\\tau_1(n,\\delta=10^{-1},\\eps=10^{-1})$" with line lt 4, "" using 1:2 title "$\\tau$ with simulations, $\\eps=10^{-1}$" with linespoints lt 4
